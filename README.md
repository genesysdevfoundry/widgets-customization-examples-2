# README #

Examples of customization with Genesys Widgets.

### Widgets Customization Examples II ###

* Quick summary

These examples have been built using Genesys Widgets 8.5.008.10.

Note that Genesys Widgets library and css are not provided in this repository, and will have to be installed separately.

### How do I get set up? ###

* Summary of set up

Get the source for the different examples in this repository.

Download the Genesys Widgets and copy the widgets.min.js into the \lib directory, and the widgets.min.css into the \stylesheets directory.
Genesys Widgets library and css are not provided in this repository.

* Configuration

At the top of the widget_init.js file, change the service URLs defined in the gmsServicesConfig object according to your environment (GMSChatURL, GMSEmailURL, GMSCallbackURL).

The WebChat, SendMessage, Callback and Channel Selector widgets are configured and enabled in the different samples.

Note that even if you don’t have a valid URL for Chat Service (or for Email/Message Service, or for Callback Service) – ex: service not configured in GMS, in your environment - you can still play with the widgets (open, close, prefill form, …).

* How to run tests

•	You can open the widget_index.html to navigate across the different customization examples, or open a sample page directly (ex: widget_sample_01.html).

### Available Customization Examples II ###

* Sample 01

Configure and enable SideBar widget.

* Sample 02

Example of a Customizable Chat Registration Form:

•	Remove Subject optional field

•	Add Topic textarea field

•	Use validate function to make email field mandatory (non empty)

* Sample 03

Example of a Customizable Chat Registration Form:

•	Anonymous Chat and use of hidden field type

•	Add AgreeWithTerms checkbox field

•	Add disclaimer

* Sample 04

Example of a Customizable Chat Registration Form:

•	Add single level drop-down list with select field type and options

•	Add two levels drop-down list with select field type and optGroup

