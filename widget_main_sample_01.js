/***********************************************************************
 * Copyright Genesys Laboratories. All Rights Reserved
 ************************************************************************/

/***********************************************************************
 * Widgets Customization Examples II - Sample 01
 * 
 * Configure and enable SideBar widget
 * 
 ************************************************************************/

var localWidgetPlugin;

// ##### Widget Configuration

initLocalWidgetConfiguration();

// Override widget configuration for SideBar

window._genesys.widgets.webchat.chatButton.enabled = false;
window._genesys.widgets.sendmessage.SendMessageButton.enabled = false;

window._genesys.widgets.main.plugins.push("cx-sidebar");

window._genesys.widgets.sidebar = {
	showOnStartup: true,
	position: 'right',
	expandOnHover: true,
	channels: [
		{
			name: 'ChannelSelector',
			clickCommand: 'ChannelSelector.open',
			readyEvent: 'ChannelSelector.ready',
			clickOptions: {},
			//use your own static string or i18n query string for the below two display properties
			displayName: 'Live Assist',
			displayTitle: 'Get live help',
			icon: 'agent'
		},
		{
			name: 'Callback',
			clickCommand: 'Callback.open',
			clickOptions: {},
			readyEvent: 'Callback.ready',
			displayName: 'Get a Call',
			displayTitle: '@i18n:callback.CallbackTitle',
			icon: 'call-incoming'
		},
		{
			name: 'WebChat',
			clickCommand: 'WebChat.open',
			clickOptions: {},
			readyEvent: 'WebChat.ready',
			displayName: 'Chat',
			displayTitle: '@i18n:webchat.ChatTitle',
			icon: 'chat'
		},
		{
			name: 'SendMessage',
			clickCommand: 'SendMessage.open',
			clickOptions: {},
			readyEvent: 'SendMessage.ready',
			displayName: 'Email',
			displayTitle: '@i18n:sidebar.SendMessageTitle',
			icon: 'email'
		}
	]
};

// ##### Widget Define Local Customization

window._genesys.widgets.onReady = function (CXBus) {
    // Use the CXBus object provided here to interface with the bus
    // CXBus here is analogous to window._genesys.widgets.bus
    localWidgetPlugin = CXBus.registerPlugin('MyLocalCustomization');
};

// ##### Import Widget Script and CSS (Widget Core, Widget Extension, Custom Override)

(function (o) {
    var f = function () {
        var d = o.location;
        o.aTags = o.aTags || [];
        for (var i = 0; i < o.aTags.length; i++) {
            var oTag = o.aTags[i];
            var fs = d.getElementsByTagName(oTag.type)[0], e;
            if (d.getElementById(oTag.id))
                return;
            e = d.createElement(oTag.type);
            e.id = oTag.id;
            if (oTag.type == "script") {
                e.src = oTag.path;
            } else {
                e.type = 'text/css';
                e.rel = 'stylesheet';
                e.href = oTag.path;
            }
            if (fs) {
                fs.parentNode.insertBefore(e, fs);
            } else {
                d.head.appendChild(e);
            }
        }
    }, ol = window.onload;
    if (o.onload) {
        typeof window.onload != "function" ? window.onload = f : window.onload = function () {
            ol();
            f();
        }
    } else
        f();
})({

    location: document,
    onload: false,
    aTags: [

        {
            type: "script",
            id: "genesys-widgets-script",
            path: "lib/widgets.min.js"
        },
        {
            type: "link",
            id: "genesys-widgets-styles",
            path: "stylesheets/widgets.min.css"
        }

    ]
});
