/***********************************************************************
 * Copyright Genesys Laboratories. All Rights Reserved
 ************************************************************************/

/***********************************************************************
 * Widgets Customization Examples II - Sample 02
 * 
 * Example of a Customizable Chat Registration Form
 * - Remove Subject optional field
 * - Add Topic textarea field
 * - Use validate function to make email field mandatory (non empty)
 * 
 ************************************************************************/
 
var localWidgetPlugin;

// ##### Widget Configuration

initLocalWidgetConfiguration();

// Override widget configuration for Customizable Chat Registration Form

window._genesys.widgets.webchat.form = {
	wrapper: "<table></table>",
	inputs: [
		{
			id: "cx_webchat_form_firstname",
			name: "firstname",
			maxlength: "100",
			placeholder: "@i18n:webchat.ChatFormPlaceholderFirstName",
			label: "@i18n:webchat.ChatFormFirstName"
		},
		{
			id: "cx_webchat_form_lastname",
			name: "lastname",
			maxlength: "100",
			placeholder: "@i18n:webchat.ChatFormPlaceholderLastName",
			label: "@i18n:webchat.ChatFormLastName"
		},
		{
			id: "cx_webchat_form_email",
			name: "email",
			maxlength: "100",
			placeholder: "Made mandatory via validate",
			label: "@i18n:webchat.ChatFormEmail",
			type: "text",
			validateWhileTyping: false, // default is false
			validate: function(event, form, input, label, $, CXBus, Common) {
				if(input) {
					if (input.val())
						return true;
					else
						return false;
				}
				return false;
			}
		},
		{
			id: "cx_webchat_form_customtopic",
			name: "customtopic",
			maxlength: "100",
			placeholder: "Optional",
			label: "Topic",
			type: "textarea"
		}
	]
};


// ##### Widget Define Local Customization

window._genesys.widgets.onReady = function (CXBus) {
    // Use the CXBus object provided here to interface with the bus
    // CXBus here is analogous to window._genesys.widgets.bus
    localWidgetPlugin = CXBus.registerPlugin('MyLocalCustomization');
};

// ##### Import Widget Script and CSS (Widget Core, Widget Extension, Custom Override)

(function (o) {
    var f = function () {
        var d = o.location;
        o.aTags = o.aTags || [];
        for (var i = 0; i < o.aTags.length; i++) {
            var oTag = o.aTags[i];
            var fs = d.getElementsByTagName(oTag.type)[0], e;
            if (d.getElementById(oTag.id))
                return;
            e = d.createElement(oTag.type);
            e.id = oTag.id;
            if (oTag.type == "script") {
                e.src = oTag.path;
            } else {
                e.type = 'text/css';
                e.rel = 'stylesheet';
                e.href = oTag.path;
            }
            if (fs) {
                fs.parentNode.insertBefore(e, fs);
            } else {
                d.head.appendChild(e);
            }
        }
    }, ol = window.onload;
    if (o.onload) {
        typeof window.onload != "function" ? window.onload = f : window.onload = function () {
            ol();
            f();
        }
    } else
        f();
})({

    location: document,
    onload: false,
    aTags: [

        {
            type: "script",
            id: "genesys-widgets-script",
            path: "lib/widgets.min.js"
        },
        {
            type: "link",
            id: "genesys-widgets-styles",
            path: "stylesheets/widgets.min.css"
        }

    ]
});




