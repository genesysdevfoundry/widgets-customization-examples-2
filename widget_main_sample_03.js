/***********************************************************************
 * Copyright Genesys Laboratories. All Rights Reserved
 ************************************************************************/

/***********************************************************************
 * Widgets Customization Examples II - Sample 03
 * 
 * Example of a Customizable Chat Registration Form
 * - Anonymous Chat and use of hidden field type
 * - Add AgreeWithTerms checkbox field
 * - Add disclaimer
 * 
 ************************************************************************/

var localWidgetPlugin;

// ##### Widget Configuration

initLocalWidgetConfiguration();

// Override widget configuration for Customizable Chat Registration Form

window._genesys.widgets.webchat.form = {
	wrapper: "<table></table>",
	inputs: [
		{
			id: "cx_webchat_form_nickname", 
			name: "nickname", 
			value: "anonymous",
			type: "hidden",
		},
		{
			id: "cx_webchat_form_email",
			name: "email",
			maxlength: "100",
			placeholder: "@i18n:webchat.ChatFormPlaceholderEmail",
			label: "@i18n:webchat.ChatFormEmail",
			title: "Enter your email address",
			type: "text"
		},
		{
			id: "cx_webchat_form_subject",
			name: "subject",
			maxlength: "100",
			placeholder: "@i18n:webchat.ChatFormPlaceholderSubject",
			label: "@i18n:webchat.ChatFormSubject"
		},
		{
			id: "cx_webchat_form_customterms", 
			name: "customterms", 
			type: "checkbox",
			title: "Agree with terms and conditions",
			label: "Terms",
			
			validateWhileTyping: false, // default is false
			validate: function(event, form, input, label, $, CXBus, Common) {
				if(input) {
					if (input.is(':checked'))
						return true;
					else
						return false;
				}
				return false;
			}
		},
		{
			custom: "<tr><td colspan='2' class='i18n' data-message='Adding a disclaimer just because I can.<br>You must agree with Terms and Conditions whatever they are.' style='font-size:12px'></td></tr>"
		}
	]
};


// ##### Widget Define Local Customization

window._genesys.widgets.onReady = function (CXBus) {
    // Use the CXBus object provided here to interface with the bus
    // CXBus here is analogous to window._genesys.widgets.bus
    localWidgetPlugin = CXBus.registerPlugin('MyLocalCustomization');
};

// ##### Import Widget Script and CSS (Widget Core, Widget Extension, Custom Override)

(function (o) {
    var f = function () {
        var d = o.location;
        o.aTags = o.aTags || [];
        for (var i = 0; i < o.aTags.length; i++) {
            var oTag = o.aTags[i];
            var fs = d.getElementsByTagName(oTag.type)[0], e;
            if (d.getElementById(oTag.id))
                return;
            e = d.createElement(oTag.type);
            e.id = oTag.id;
            if (oTag.type == "script") {
                e.src = oTag.path;
            } else {
                e.type = 'text/css';
                e.rel = 'stylesheet';
                e.href = oTag.path;
            }
            if (fs) {
                fs.parentNode.insertBefore(e, fs);
            } else {
                d.head.appendChild(e);
            }
        }
    }, ol = window.onload;
    if (o.onload) {
        typeof window.onload != "function" ? window.onload = f : window.onload = function () {
            ol();
            f();
        }
    } else
        f();
})({

    location: document,
    onload: false,
    aTags: [

        {
            type: "script",
            id: "genesys-widgets-script",
            path: "lib/widgets.min.js"
        },
        {
            type: "link",
            id: "genesys-widgets-styles",
            path: "stylesheets/widgets.min.css"
        }

    ]
});




