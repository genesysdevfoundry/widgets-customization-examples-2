/***********************************************************************
 * Copyright Genesys Laboratories. All Rights Reserved
 ************************************************************************/

/***********************************************************************
 * Widgets Customization Examples II - Sample 04
 * 
 * Example of a Customizable Chat Registration Form
 * - Add single level drop-down list with select field type and options
 * - Add two levels drop-down list with select field type and optGroup
 * 
 ************************************************************************/
 
var localWidgetPlugin;

// ##### Widget Configuration

initLocalWidgetConfiguration();

// Override widget configuration for Customizable Chat Registration Form

window._genesys.widgets.webchat.form = {
	wrapper: "<table></table>",
	inputs: [
		{
			id: "cx_webchat_form_firstname",
			name: "firstname",
			maxlength: "100",
			placeholder: "@i18n:webchat.ChatFormPlaceholderFirstName",
			label: "@i18n:webchat.ChatFormFirstName"
		},
		{
			id: "cx_webchat_form_lastname",
			name: "lastname",
			maxlength: "100",
			placeholder: "@i18n:webchat.ChatFormPlaceholderLastName",
			label: "@i18n:webchat.ChatFormLastName"
		},
		{
			id: "cx_webchat_form_email",
			name: "email",
			maxlength: "100",
			placeholder: "Made mandatory via validate",
			label: "@i18n:webchat.ChatFormEmail",
			type: "text",
			validateWhileTyping: false, // default is false
			validate: function(event, form, input, label, $, CXBus, Common) {
				if(input) {
					if (input.val())
						return true;
					else
						return false;
				}
				return false;
			}
		},
		{
			id: "cx_webchat_form_customsubject1",
			name: "customsubject1",
			placeholder: "@i18n:webchat.ChatFormPlaceholderSubject",
			label: "Subject 1",
			type: "select",
			options: [
				{
					disabled : "disabled",
					selected : "selected",
					hidden: "hidden"
				},
				{
					text : "Text 1",
					value : "value1"
				},
				{
					text : "Text 2",
					value : "value2"
				}
			],
			wrapper: "<tr><th>{label}</th><td>{input}</td></tr>"
		},
		{
			id: "cx_webchat_form_customsubject2",
			name: "customsubject2",
			placeholder: "@i18n:webchat.ChatFormPlaceholderSubject",
			label: "Subject 2",
			type: "select",
			options: [
				{
					disabled : "disabled",
					selected : "selected",
					hidden: "hidden"
				},
				{
					group: true,
					text : "Level 1"
				},
				{
					text : "Text 1",
					value : "value1"
				},
				{
					text : "Text 2",
					value : "value2"
				},
				{
					group: true,
					text : "Level 2"
				},
				{
					text : "Text 3",
					value : "value3"
				},
				{
					text : "Text 4",
					value : "value4"
				}
			],
			wrapper: "<tr><th>{label}</th><td>{input}</td></tr>"
		}
	]
};


// ##### Widget Define Local Customization

window._genesys.widgets.onReady = function (CXBus) {
    // Use the CXBus object provided here to interface with the bus
    // CXBus here is analogous to window._genesys.widgets.bus
    localWidgetPlugin = CXBus.registerPlugin('MyLocalCustomization');
};

// ##### Import Widget Script and CSS (Widget Core, Widget Extension, Custom Override)

(function (o) {
    var f = function () {
        var d = o.location;
        o.aTags = o.aTags || [];
        for (var i = 0; i < o.aTags.length; i++) {
            var oTag = o.aTags[i];
            var fs = d.getElementsByTagName(oTag.type)[0], e;
            if (d.getElementById(oTag.id))
                return;
            e = d.createElement(oTag.type);
            e.id = oTag.id;
            if (oTag.type == "script") {
                e.src = oTag.path;
            } else {
                e.type = 'text/css';
                e.rel = 'stylesheet';
                e.href = oTag.path;
            }
            if (fs) {
                fs.parentNode.insertBefore(e, fs);
            } else {
                d.head.appendChild(e);
            }
        }
    }, ol = window.onload;
    if (o.onload) {
        typeof window.onload != "function" ? window.onload = f : window.onload = function () {
            ol();
            f();
        }
    } else
        f();
})({

    location: document,
    onload: false,
    aTags: [

        {
            type: "script",
            id: "genesys-widgets-script",
            path: "lib/widgets.min.js"
        },
        {
            type: "link",
            id: "genesys-widgets-styles",
            path: "stylesheets/widgets.min.css"
        }

    ]
});




